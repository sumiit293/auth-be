

module.exports = constants = {
    ROLE_NAMES: {
        authenticated: 'Authenticated',
        superAdmin: 'Super Admin',
        public: 'Public',
    },
}