const { addColors, format: _format, transports: _transports, createLogger } = require('winston');

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  http: 3,
  debug: 4,
}

const level = () => {
  const env = process.env.NODE_ENV || 'development'
  const isDevelopment = env === 'development'
  return isDevelopment ? 'debug' : 'warn'
}

const colors = {
  error: 'red',
  warn: 'yellow',
  info: 'green',
  http: 'magenta',
  debug: 'white',
}

addColors(colors)

const consoleFormat = _format.combine(
  _format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
  _format.colorize({ all: true }),
  _format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`,
  ),
)

const fileFormat = _format.combine(
  _format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
  _format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`,
  ),
)

const transports = [
  new _transports.Console({
    format: consoleFormat
  }),
  new _transports.File({
    filename: 'logs/error.log',
    level: 'error',
    format: fileFormat
  }),
  new _transports.File({ filename: 'logs/all.log', format: fileFormat }),
]

const Logger = createLogger({
  level: level(),
  levels,
  transports,
})

module.exports = Logger;