const jwt = require('jsonwebtoken')
const User = require('../../api/users/user');
const Permission = require('../../api/permissions/permission');
const Role = require('../../api/roles/role');
const { ROLE_NAMES } = require('../../shared/constants');

const auth = async (req, res, next) => {
    try {

        const token = req.header('Authorization').replace('Bearer ', '')
        const decoded = jwt.verify(token, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })
            .select('+tokens')
            .populate('role')
            .exec();
        if (!user) {
            throw new Error('No such user found')
        }
        req.token = token
        req.user = user
        next();

    } catch (e) {
        res.status(401).send({ error: 'Please Authenticate' })
    }
}

const authorize = (action, module) => {
    return async (req, res, next) => {
        try {
            let roleId;
            if (module === 'public') {
                const role = await Role.findOne({name: ROLE_NAMES.public});
                roleId = role.id;
            } else {
                roleId = req.user.role.id;
            }
            const permission = await Permission.findOne({ role: roleId });
            if (!permission) {
                res.status(401).send({ error: 'Authorization failed' })
            }
            const actionsArr = permission.permissions.filter(perm => {
                return perm.module === module
            })
            const actionObj = actionsArr[0].actions.filter(actn => {
                return Object.keys(actn)[0] === action;
            })
            if (Object.values(actionObj[0])[0] === true) {
                next()
            } else {
                res.status(401).send({ error: 'Not authorized' })
            }

        } catch (e) {
            res.status(401).send({ error: 'Authorization failed' })
        }
    }
}


module.exports = {
    auth,
    authorize
}