
module.exports = {
    permissionActions: [
        {
            module: 'users',
            actions: [
                'userLogout',
                'userLogoutAll',
                'getAllUsers',
                'getUser',
                'userUpdate',
                'userDelete'
            ]
        },
        {
            module: 'roles',
            actions: [
                'createRole',
                'getRole',
                'getAllRoles',
                'updateRole',
                'deleteRole',
            ]
        },
        {
            module: 'permissions',
            actions: [
                'createPermission',
                'getPermission',
                'getAllPermissions',
                'getStaticPermissions',
                'asdasdasdas',
                'getPermissionsByRole',
                'updatePermission',
                'deletePermission',
            ]
        },
        {
            module: 'category',
            actions: [
                'createCategory',
                'updateCategory',
                'deleteCategory',
            ]
        },
        {
            module: 'product',
            actions: [
                'createProduct',
                'updateProduct',
            ]
        },
        {
            module: 'public',
            actions: [
                'superAdminRegister',
                'addUser',
                'superAdminLogIn',
                'userLogIn',
                'getCategory',
                'getAllCategory',
                'getProduct',
                'getAllProduct'
            ]
        }
    ],
    publicActions: [
        'superAdminRegister',
        'addUser',
        'superAdminLogIn',
        'userLogIn',
        'getCategory',
        'getAllCategory',
        'getProduct',
        'getAllProduct'
    ]
}
