const mongoose = require('mongoose');
const validator = require('validator').default;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
	role: {
		type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
        required: true,
        trim: true
	},
    email: {
		type: String,
		trim: true,
		validate(value) {
			if(!validator.isEmail(value)) {
				throw new Error('Email is invalid')
			}
		}
	},
    password: {
		type: String,
		trim: true,
		validate(value) {
			if(validator.isAlpha(value)) {
				throw new Error("Password should contain atleast 1 numeric value")
			} else if(value.toLowerCase().includes("password")) {
				throw new Error("Password should not contain 'password'")
			}
		},
		select: false
	},
	tokens: {
		type: [{
			token: {
				type: String,
				required: true,
			}
		}],
		select: false
	}
}, {timestamps: true})

userSchema.methods.generateAuthToken = async function () {
	const user = await User.findOne({ _id: this._id }).select("+tokens");
	const token = jwt.sign({ user: 'user', _id: user._id.toString() }, process.env.JWT_SECRET)
	user.tokens = user.tokens.concat({token: token})
	await user.save()
	return token
}

userSchema.statics.findByCredentials = async function (email, password) {
	const user = await User.findOne({ email: email }).select("+password").populate('role').exec();
	if (!user) {
		throw new Error('unable to login')
	}
	const isMatch = await bcrypt.compare(password, user.password)
	if (!isMatch) {
		throw new Error('unable to login')
	}
	return user
}

userSchema.pre('save', async function(next) {
	const user = this
	if(user.isModified('password')) {
		user.password = await bcrypt.hash(user.password, 8)
	}
	next()
})

userSchema.pre('findOneAndUpdate', async function(next) {
	const user = this
	if(user._update.password) {
		user._update.password = await bcrypt.hash(user._update.password, 8)
	}
	next()
})

userSchema.methods.removePassToken = function () {
	const user = this
	const userObject = user.toObject()
	delete userObject.tokens
	if (user.password) {
		delete userObject.password
	}
	return userObject
}

const User = mongoose.model('User', userSchema);

module.exports = User;