const express = require('express');
const router = express.Router();
const User = require('./user');
const Logger = require('../../shared/middlewares/logger');
const Role = require('../roles/role');
const { ROLE_NAMES } = require('../../shared/constants');
const { createSuperAdminRoleAndPermissions, createPublicPermissions } = require('./user-services');
const Permission = require('../permissions/permission');

module.exports = {
    async isRegistered(req, res, next) {
        try {

            const role = await Role.findOne({ name: ROLE_NAMES.superAdmin })
            console.log(role)
            if (role) {
                return res.status(200).json({
                    message: 'Super admin registered',
                    registered: true
                });
            }
            res.status(200).json({
                message: 'Super admin not registered',
                registered: false
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async superAdminRegister(req, res, next) {
        try {

            const role = await Role.findOne({ name: ROLE_NAMES.superAdmin })
            if (role) {
                Logger.info('Super admin exists - Permission denied');
                return res.status(400).json({
                    message: 'Permission denied'
                });
            }
            const { superAdminRole, superAdminPermission } = await createSuperAdminRoleAndPermissions();
            const { publicRole, publicPermission } = await createPublicPermissions();

            const superAdminUser = new User({ ...req.body, role: superAdminRole.id });
            await superAdminUser.save();
            Logger.info(`Super admin user created - ${superAdminUser.id}`);

            const user = await User.find({role: superAdminRole.id}).populate('role').exec();

            const token = await superAdminUser.generateAuthToken()
            res.status(201).json({
                message: 'Super admin Created',
                user: user[0].removePassToken(),
                token: token
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async addUser(req, res, next) {
        try {

            const user1 = await User.find({ email: req.body.email })
            if (user1.length > 0) {
                Logger.info(`${req.body.email} - Email Exists`);
                return res.status(409).json({
                    message: 'Email Exists'
                });
            }
            let user;
            if (req.body.role) {
                user = new User(req.body)
            } else {
                let role = await Role.findOne({ name: ROLE_NAMES.authenticated })
                if (!role) {
                    role = new Role({ name: ROLE_NAMES.authenticated })
                    role.save();
                    Logger.info(`Authenticated role created - ${role.id}`);
                }
                user = new User({ ...req.body, role: role.id })
            }
            await user.save()
            const token = await user.generateAuthToken()
            res.status(201).json({
                message: 'User Created',
                user: user.removePassToken(),
                token: token
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async superAdminLogIn(req, res, next) {
        try {

            const user = await User.findByCredentials(req.body.email, req.body.password)
            const role = await Role.findById(user.role);
            if (role.name !== ROLE_NAMES.superAdmin) {
                Logger.info(`${req.body.email} - Not a Super Admin`);
                return res.status(400).json({
                    message: 'Permission denied'
                });
            }
            const token = await user.generateAuthToken()
            return res.status(200).json({
                message: 'Auth Successfull',
                user: user.removePassToken(),
                token: token
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async userLogIn(req, res, next) {
        try {

            const user = await User.findByCredentials(req.body.email, req.body.password)
            const p = await Permission.find({role: user.role._id})
            const token = await user.generateAuthToken()
            return res.status(200).json({
                message: 'Auth Successfull',
                user: user.removePassToken(),
                token: token,
                permissions: [...p.map((role)=>role.permissions)]
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async userLogout(req, res, next) {
        try {

            const user = req.user
            user.tokens = user.tokens.filter((token) => {
                return token.token !== req.token
            })
            await user.save()
            return res.status(200).json({
                message: 'Logout Successfull'
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async userLogoutAll(req, res, next) {
        try {

            const user = req.user
            user.tokens = []
            await user.save()
            return res.status(200).json({
                message: 'Logout of All Sessions Successfull'
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getUser(req, res, next) {
        try {

            const user = await User.findById(req.params.id)
            if (!user) {
                Logger.info(`${req.params.id} - User not found`);
                return res.status(409).json({
                    message: 'User not found'
                });
            }
            res.status(200).json({
                user: user
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getAllUsers(req, res, next) {
        try {

            const user = await User.find(req.query)
            res.status(200).json({
                user: user
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async userUpdate(req, res, next) {
        try {

            const user = await User.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
            return res.status(200).json({
                message: 'Update Successfull',
                user: user.removePassToken()
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async userDelete(req, res, next) {
        try {

            const user = await User.findByIdAndDelete(req.params.id)
            return res.status(200).json({
                message: 'Delete Successfull',
                user: user.removePassToken()
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },
}