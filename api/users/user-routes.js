const express = require('express');
const router = express.Router();
const { auth, authorize } = require('../../shared/middlewares/auth')
const UserController = require('./user-controller');

router.post('/register', UserController.superAdminRegister);

router.get('/superadmincheck', UserController.isRegistered);

router.post('/superadminlogin', authorize('superAdminLogIn', 'public'), UserController.superAdminLogIn);

router.post('/login', authorize('userLogIn', 'public'), UserController.userLogIn);

router.post('/add', authorize('addUser', 'public'), UserController.addUser);

router.post('/logout', auth, authorize('userLogout', 'users'), UserController.userLogout);

router.post('/logoutall', auth, authorize('userLogoutAll', 'users'), UserController.userLogoutAll);

router.get('/user', auth, authorize('getAllUsers', 'users'), UserController.getAllUsers);

router.get('/user/:id', auth, authorize('getUser', 'users'), UserController.getUser);

router.put('/user/:id', auth, authorize('userUpdate', 'users'), UserController.userUpdate);

router.delete('/user/:id', auth, authorize('userDelete', 'users'), UserController.userDelete);

module.exports = router;
