const User = require('./user');
const Logger = require('../../shared/middlewares/logger');
const Role = require('../roles/role');
const Permission = require('../permissions/permission');
const { ROLE_NAMES } = require('../../shared/constants');
const { permissionActions, publicActions } = require('../../shared/controller-actions');

module.exports = {
    async createSuperAdminRoleAndPermissions() {
        try {

            const superAdminRole = new Role({ name: ROLE_NAMES.superAdmin })
            await superAdminRole.save();
            Logger.info(`Super admin role created - ${superAdminRole.id}`);

            const permissionsArr = permissionActions.map(module => {
                const actionsArr = module.actions.map(act => {
                    return {
                        [act]: true
                    }
                })
                return {
                    module: module.module,
                    actions: actionsArr
                }
            })
            console.log(permissionsArr)
            const superAdminPermission = new Permission({
                role: superAdminRole.id,
                permissions: permissionsArr
            })
            await superAdminPermission.save();
            Logger.info(`Super admin permission created - ${superAdminPermission.id}`);

            return {
                superAdminRole,
                superAdminPermission
            }

        } catch (err) {
            Logger.error(err.toString());
        }
    },

    async createPublicPermissions() {
        try {

            const publicRole = new Role({ name: ROLE_NAMES.public })
            await publicRole.save();
            Logger.info(`Public role created - ${publicRole.id}`);
            const publicactions = publicActions.map(act => {
                return {
                    [act]: true
                }
            })
            const permissionsArr = [
                {
                    module: 'public',
                    actions: publicactions
                }
            ]
            const publicPermission = new Permission({
                role: publicRole.id,
                permissions: permissionsArr
            })
            await publicPermission.save();
            Logger.info(`Public permission created - ${publicPermission.id}`);

            return {
                publicRole,
                publicPermission
            }

        } catch (err) {
            Logger.error(err.toString());
        }
    }
}