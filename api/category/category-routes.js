const express = require('express');
const router = express.Router();
const { auth, authorize } = require('../../shared/middlewares/auth')

const CategoryController = require('./category-controller');

router.post('/', auth, authorize('createCategory', 'category'), CategoryController.createCategory);

router.get('/:id', CategoryController.getCategory);

router.get('/',CategoryController.getAllCategory);

router.put('/:id', auth, authorize('updateCategory', 'category'), CategoryController.updateCategory);

router.delete('/:id', auth, authorize('deleteCategory', 'category'),CategoryController.deleteCategory);

module.exports = router;