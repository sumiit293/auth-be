const Logger = require('../../shared/middlewares/logger');
const Category = require('./category');
const Product = require('../product/product');

module.exports = {
    async createCategory(req, res, next) {
        try {
            const role = await Category.find({ name: req.body.name })
            if (role.length > 0) {
                Logger.info(`${req.body.name} - Category with given name aleady Exists`);
                return res.status(409).json({
                    message: 'Category with given name aleady Exists'
                });
            }
            const newCategory = new Category(req.body)
            await newCategory.save()
            res.status(201).json({
                message: 'Category Created',
                category: newCategory
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getCategory(req, res, next) {
        try {

            const category = await Category.findById(req.params.id)
            if (!category) {
                Logger.info(`${req.params.id} - Category not found`);
                return res.status(409).json({
                    message: 'Category not found'
                });
            }
            res.status(200).json({
                message: 'Category found',
                category: category
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getAllCategory(req, res, next) {
        try {
            const category = await Category.find(req.query)
            res.status(200).json({
                categories: category
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async updateCategory(req, res, next) {
        try {
            const updatedCategory = await Category.findByIdAndUpdate(req.params.id, req.body, { new: true })
            return res.status(200).json({
                message: 'Update Successful',
                category: updatedCategory
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async deleteCategory(req, res, next) {
        try {
            const products = await Product.find({ role: req.params.id })
            if (products.length > 0) {
                Logger.info(`${req.params.id} - Product with given category exists`);
                return res.status(409).json({
                    message: 'Product with given category exists'
                });
            }
            const category = await Category.findOne({ role: req.params.id });
            if (category) {
                await Category.findByIdAndDelete(category.id);
            }
            return res.status(200).json({
                message: 'Delete Successful',
                category: category
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },
}