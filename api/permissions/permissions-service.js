const Permissions = require('./permission');
const Logger = require('../../shared/middlewares/logger');
const { permissionActions } = require('../../shared/controller-actions');

module.exports = {
    async getRolePermissionsWithNewAddedPermissions(roleId) {
        try {

            const permissions = await Permissions.find({ role: roleId })
            let updatedPermissions = permissions[0];
            let updatedPerms;
            if (permissions.length) {
                const staticPermissions = permissionActions
                const moduleDiff = staticPermissions.filter((item1) =>
                    !updatedPermissions.permissions.some((item2) => (item2.module === item1.module)))
                if (moduleDiff.length) {
                    const modifiedPerms = moduleDiff.map(module => {
                        const modifiedActions = module.actions.map((actn) => {
                            return { [actn]: false };
                        })
                        return { ...module, actions: modifiedActions }
                    })
                    updatedPerms = [...permissions[0].permissions, ...modifiedPerms]
                    updatedPermissions = { ...updatedPermissions._doc, permissions: updatedPerms }
                }
                const actionsDiff = staticPermissions.filter((item1) =>
                    !updatedPermissions.permissions.some((item2) => (item2.actions.length === item1.actions.length)))
                if (actionsDiff.length) {
                    const modifiedActionsPerms = actionsDiff.map(module => {
                        const obj = updatedPermissions.permissions.find(mod => mod.module === module.module)
                        const actionDiffs = module.actions.filter((item1) =>
                            !obj._doc.actions.some((item2) => (Object.keys(item2).toString() === item1)))
                        const modifiedActnsDiffs = actionDiffs.map(actn => ({ [actn]: false }))
                        return {
                            ...obj._doc,
                            actions: [...obj._doc.actions, ...modifiedActnsDiffs]
                        }
                    })[0]
                    const index = updatedPermissions.permissions.findIndex(mod => mod.module === modifiedActionsPerms.module)
                    updatedPermissions.permissions[index] = modifiedActionsPerms;
                }
            }
            return updatedPermissions

        } catch (err) {
            Logger.error(err.toString());
        }
    },
}

