const express = require('express');
const router = express.Router();
const Permissions = require('./permission');
const Logger = require('../../shared/middlewares/logger');
const { permissionActions, publicActions } = require('../../shared/controller-actions');
const { getRolePermissionsWithNewAddedPermissions } = require('./permissions-service')

module.exports = {
    async createPermission(req, res, next) {
        try {

            const permission = await Permissions.find({ role: req.body.role })
            if (permission.length > 0) {
                Logger.info(`${req.body.role} - Permission for given role aleady Exists`);
                return res.status(409).json({
                    message: 'Permission for given role aleady Exists'
                });
            }
            const newPermission = new Permissions(req.body)
            await newPermission.save()
            res.status(201).json({
                message: 'Permission Created',
                permission: newPermission
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getPermission(req, res, next) {
        try {

            const permission = await Permissions.findById(req.params.id)
            if (!permission) {
                Logger.info(`${req.params.id} - Permission not found`);
                return res.status(409).json({
                    message: 'Permission not found'
                });
            }
            res.status(200).json({
                permissions: permission
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getPermissionsByRole(req, res, next) {
        try {

            const permissions = await getRolePermissionsWithNewAddedPermissions(req.params.id)
            res.status(200).json({
                permissions: permissions
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getAllPermissions(req, res, next) {
        try {

            const permissions = await Permissions.find(req.query)
            res.status(200).json({
                permissions: permissions
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getStaticPermissions(req, res, next) {
        try {

            const modifiedPermissions = permissionActions.map((module) => {
                const modifiedActions = module.actions.map((actn) => {
                    if (module.module === 'public') {
                        return { [actn]: true }
                    } else {
                        return { [actn]: false }
                    }
                })
                return { ...module, actions: modifiedActions }
            })

            res.status(200).json({
                permissions: modifiedPermissions
            });

        } catch (err) {
            console.log(err)
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async updatePermission(req, res, next) {
        try {

            const permission = await Permissions.findByIdAndUpdate(req.params.id, req.body, { new: true })
            return res.status(200).json({
                message: 'Update Successful',
                permissions: permission
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async deletePermission(req, res, next) {
        try {

            const permissions = await Permissions.findByIdAndDelete(req.params.id)
            return res.status(200).json({
                message: 'Delete Successful',
                permissions: permissions
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },
}

