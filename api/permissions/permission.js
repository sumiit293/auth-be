const mongoose = require('mongoose');
const validator = require('validator');
const Logger = require('../../shared/middlewares/logger');
const mongooseConnection = require('../../server');
const Role = require('../roles/role');

const permissionSchema = new mongoose.Schema({
    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
        required: true,
        trim: true
    },
    permissions: [
        {
            module: {
                type: String,
                required: true,
                trim: true
            },
            actions:[
                {
                    type: Object,
                    trim: true
                }
            ]
        }
    ]
})

// mongooseConnection.then(() => {
//     mongoose.connection.db.listCollections().toArray(async function (err, names) {
//         if (err) {
//             Logger.error(err);
//         }
//         else {
//             let roles = await Role.find();
//             if (roles.length <= 0) {
//                 const newRole = new Role();
//                 await newRole.save();
//                 Logger.info(`Permission creation - new default role created >> ${newRole.name}`);
//                 roles = await Role.find();
//             }
//             roles.forEach(role => {
//                 names.forEach(async function (e, i, a) {
//                     const permission = await Permission.find({ role: role.id, module: e.name })
//                     if (e.name === 'permissions' || e.name === 'roles') {
//                         return;
//                     } else if (permission.length > 0) {
//                         return;
//                     } else {
//                         const newPermission = new Permission({
//                             role: role.id,
//                             module: e.name,
//                             permissions: ['POST', 'PUT', 'GET', 'DELETE']
//                         });
//                         await newPermission.save();
//                         console.log(newPermission)
//                         Logger.info(`Permission created >> ${newPermission.module}`);
//                     }
//                 });
//             })
//         }
//     });
// })

const Permission = mongoose.model('Permission', permissionSchema);

module.exports = Permission;