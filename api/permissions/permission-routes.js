const express = require('express');
const router = express.Router();
const { auth, authorize } = require('../../shared/middlewares/auth')
const PermissionController = require('./permission-controller');

router.post('/add', auth, authorize('createPermission', 'permissions'), PermissionController.createPermission);

router.get('/static', auth, PermissionController.getStaticPermissions);

router.get('/:id', auth, authorize('getPermission', 'permissions'), PermissionController.getPermission);

router.get('/role/:id', auth, authorize('getPermissionsByRole', 'permissions'), PermissionController.getPermissionsByRole);

router.get('/', auth, authorize('getAllPermissions', 'permissions'), PermissionController.getAllPermissions);

router.put('/:id', auth, authorize('updatePermission', 'permissions'), PermissionController.updatePermission);

router.delete('/:id', auth, authorize('deletePermission', 'permissions'), PermissionController.deletePermission);

module.exports = router;

