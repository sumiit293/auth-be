const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    imgURL:{
        type: String,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    currPrice: {
        type: Number,
        required: true
    },
    salePrice: {
        type: Number,
        required: true
    },
    category: {
		type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
        trim: true
	}
},{timestamps: true});

const Product = mongoose.model('Product', productSchema);
module.exports = Product;