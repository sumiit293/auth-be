const Logger = require('../../shared/middlewares/logger');
const Product = require('../product/product');
const Category = require('./../category/category');


module.exports = {
    async createProduct(req, res, next) {
        try {
            
            const newProduct = new Product(req.body);
            await newProduct.save();
            res.status(201).json({
                message: 'Product Created',
                product: newProduct
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async updateProduct(req, res, next) {
        try {
            const updatedProduct = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true })
            return res.status(200).json({
                message: 'Update Successful',
                product: updatedProduct
            })
        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getAllProduct(req, res, next) {
        try {
            const product = await Product.find(req.query).populate('category').exec()
            res.status(200).json({
                products: product
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getProduct(req, res, next) {
        try {
            const product = await Product.findById(req.params.id);

            if(product != null){
                res.status(200).json({
                    product: product
                });
            }else{
                Logger.info(`${req.params.id} - Product not found`);
                return res.status(409).json({
                    message: 'Product not found'
                });
            }
        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    }
}