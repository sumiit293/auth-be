const express = require('express');
const router = express.Router();
const ProductController = require('./product-controller');
const { auth, authorize } = require('../../shared/middlewares/auth')


router.post('/',auth, authorize('createProduct', 'product'), ProductController.createProduct);

router.get('/:id', ProductController.getProduct);

router.get('/',ProductController.getAllProduct);

router.put('/:id',auth, authorize('updateProduct', 'product'),ProductController.updateProduct);

module.exports = router;