const express = require('express');
const router = express.Router();
const { auth, authorize } = require('../../shared/middlewares/auth')
const RoleController = require('./role-controller');

router.post('/add', auth, authorize('createRole', 'roles'), RoleController.createRole);

router.get('/:id', auth, authorize('getRole', 'roles'), RoleController.getRole);

router.get('/', auth, authorize('getAllRoles', 'roles'), RoleController.getAllRoles);

router.put('/:id', auth, authorize('updateRole', 'roles'), RoleController.updateRole);

router.delete('/:id', auth, authorize('deleteRole', 'roles'), RoleController.deleteRole);

module.exports = router;