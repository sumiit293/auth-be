const express = require('express');
const router = express.Router();
const Role = require('./role');
const Logger = require('../../shared/middlewares/logger');
const User = require('../users/user');
const Permissions = require('../permissions/permission');

module.exports = {
    async createRole(req, res, next) {
        try {

            const role = await Role.find({ name: req.body.name })
            if (role.length > 0) {
                Logger.info(`${req.body.name} - Role with given name aleady Exists`);
                return res.status(409).json({
                    message: 'Role with given name aleady Exists'
                });
            }
            const newRole = new Role(req.body)
            await newRole.save()
            res.status(201).json({
                message: 'Role Created',
                role: newRole
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getRole(req, res, next) {
        try {

            const role = await Role.findById(req.params.id)
            if (!role) {
                Logger.info(`${req.params.id} - Role not found`);
                return res.status(409).json({
                    message: 'Role not found'
                });
            }
            res.status(200).json({
                message: 'Role found',
                role: role
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async getAllRoles(req, res, next) {
        try {

            const roles = await Role.find(req.query)
            const formattedRoles = roles.filter(role => {
                return role.name !== 'superadmin' && role.name !== 'user';
            })
            res.status(200).json({
                roles: formattedRoles
            });

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async updateRole(req, res, next) {
        try {

            const role = await Role.findByIdAndUpdate(req.params.id, req.body, { new: true })
            return res.status(200).json({
                message: 'Update Successful',
                role: role
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async deleteRole(req, res, next) {
        try {

            const users = await User.find({ role: req.params.id })
            if (users.length > 0) {
                Logger.info(`${req.params.id} - Users with given role exists`);
                return res.status(409).json({
                    message: 'Users with given role exists'
                });
            }
            const permissions = await Permissions.findOne({ role: req.params.id });
            if (permissions) {
                await Permissions.findByIdAndDelete(permissions.id)
            }
            const role = await Role.findByIdAndDelete(req.params.id)
            return res.status(200).json({
                message: 'Delete Successful',
                role: role
            })

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },
}