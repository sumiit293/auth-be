const express = require('express');
const router = express.Router();

module.exports = {
    async showLoginPage(req, res, next) {
        try {
            
            res.render('login')

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },

    async show404Page(req, res, next) {
        try {

            res.render('404')

        } catch (err) {
            Logger.error(err.toString());
            res.status(500).json({
                error: err
            });
        }
    },
}