const express = require('express');
const router = express.Router();
const ViewsController = require('./views-controller');

router.get('/login', ViewsController.showLoginPage);

router.use(ViewsController.show404Page);

module.exports = router;