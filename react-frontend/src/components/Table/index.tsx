import React, { useState } from 'react';
import { useTheme } from '@mui/material';
import Table from '@mui/material/Table';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import TextField from '@mui/material/TextField';
import Box from '@mui/system/Box';

type Order = 'asc' | 'desc';

const styles = (theme: any) => (
    {
        filter: {
            [theme.breakpoints.up('md')]: {
                width: '40%',
            }
        },
        box: {
            display: 'flex',
            alignItems: 'right',
            justifyContent: 'right'
        },
        tblhead: {
            backgroundColor: 'rgb(226 226 226 / 87%)',
        },
        tbl: {
            '& tbody tr:hover': {
                backgroundColor: 'rgb(228 228 228 / 43%)',
                cursor: 'pointer',
            },
        }
    }
)

export default function useTable(records: any = [], headcells: any = []) {

    const pages = [5, 10, 25];
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(pages[1])
    const [order, setOrder] = useState<Order>('asc');
    const [orderBy, setOrderBy] = useState('id');

    const theme = useTheme();
    const style = styles(theme);

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }

    function getComparator<Key extends keyof any>(
        order: Order,
        orderBy: Key,
    ): (
            a: { [key in Key]: number | string },
            b: { [key in Key]: number | string },
        ) => number {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }

    function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
        const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) {
                return order;
            }
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    const recordsAfterPagingAndSorting = (): any => {
        return stableSort(records, getComparator(order, orderBy)).slice(page * rowsPerPage, (page + 1) * rowsPerPage)
    };

    const handleRequestSort = (
        cellid: any,
    ) => {
        const isAsc = orderBy === cellid && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(cellid);
    };

    const TableDiv = (props: any) => (
        <TableContainer>
            <Table sx={style.tbl}>
                {props.children}
            </Table>
        </TableContainer>
    )

    const Filter = (props: any) => (
        <Box sx={style.box}>
            <TextField
                margin="normal"
                fullWidth
                id="search"
                label="Search"
                name="search"
                autoFocus
                sx={style.filter}
            />
        </Box>
    )

    const TableHeader = (props: any) => (
        <TableHead sx={style.tblhead}>
            <TableRow>
                {
                    headcells.map((cell: any) => (
                        <TableCell sortDirection={orderBy === cell.id ? order : false} key={cell.id}>
                            {
                                cell.disableSorting ? cell.label :
                                    <TableSortLabel
                                        active={orderBy === cell.id}
                                        direction={orderBy === cell.id ? order : 'asc'}
                                        onClick={() => handleRequestSort(cell.id)}>
                                        {cell.label}
                                    </TableSortLabel>
                            }
                        </TableCell>
                    ))
                }
            </TableRow>
        </TableHead>
    )

    const TablePaginate = (props: any) => (
        <TablePagination
            component="div"
            rowsPerPageOptions={pages}
            page={page}
            rowsPerPage={rowsPerPage}
            count={records.length}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
        />
    )


    return {
        TableDiv,
        TableHeader,
        TablePaginate,
        recordsAfterPagingAndSorting,
        Filter
    }
}

// sx={{
//     marginTop: theme.spacing(3),
//     '& thead th': {
//         fontWeight: '600'
//     },
//     '& tbody td': {
//         fontWeight: '300',
//     },
//     '& tbody tr:hover': {
//         backgroundColor: 'rgb(226 226 226 / 87%)',
//         cursor: 'pointer',
//     },
// }}