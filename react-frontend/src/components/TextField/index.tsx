import React from 'react';
import { TextField } from '@mui/material';
import { useField } from 'formik';

const TextFieldWrapper = ({ name, ...otherprops }: any) => {

    const [field, meta, helpers] = useField(name)

    const configTextField: any = {
        ...field,
        ...otherprops,
        fullWidth: true,
        variant: 'filled'
    }

    if (meta && meta.touched && meta.error) {
        configTextField.error = true;
        configTextField.helperText = meta.error;
    }

    return (
        <TextField {...configTextField} />
    )
}

export default TextFieldWrapper;
