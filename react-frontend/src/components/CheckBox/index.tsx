import React, { FunctionComponent, useState } from 'react';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import { useField, useFormikContext } from 'formik';

const CheckBoxWrapper: FunctionComponent<any> = ({ name, label, legend, defaultChecked, ...otherprops }: any) => {
    const [checked, setChecked]: any = useState(defaultChecked);

    const [field, meta, helpers] = useField(name)
    const { setFieldValue } = useFormikContext();

    const handleChange = (evt: any) => {
        const { checkedd } = evt.target;
        setChecked(checkedd)
        setFieldValue(name, checkedd);
    }

    const configCheckBox = {
        ...field,
        ...otherprops,
        value: meta.initialValue,
        onChange: handleChange
    }

    const configFormControl: any = {}

    if (meta && meta.touched && meta.error) {
        configFormControl.error = true;
    }

    return (
        <FormControl {...configFormControl}>
            <FormLabel component="legend">{legend}</FormLabel>
            <FormGroup>
                <FormControlLabel
                    control={<Checkbox {...configCheckBox} defaultChecked={checked} />}
                    label={label}
                />
            </FormGroup>
        </FormControl>
    )
}

export default CheckBoxWrapper;
