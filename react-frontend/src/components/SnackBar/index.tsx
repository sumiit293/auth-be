import React, { FunctionComponent } from 'react';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';

const SnackBar: FunctionComponent<any> = (props: any) => {
    const { open, type, handleClose, message } = props;

    const onClose = () => {
        handleClose();
    }

    const defaultConfigs = {
        autoHideDuration: 5000,
        onClose: onClose
    }

    return (
        <Snackbar open={open} {...defaultConfigs}>
            {
                type === 'success' ?
                    <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                        {message}
                    </Alert>
                    :
                    type === 'error' ?
                        <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                            {message}
                        </Alert>
                        :
                        type === 'warning' ?
                            <Alert onClose={handleClose} severity="warning" sx={{ width: '100%' }}>
                                {message}
                            </Alert>
                            :
                            type === 'info' ?
                                <Alert onClose={handleClose} severity="info" sx={{ width: '100%' }}>
                                    {message}
                                </Alert>
                                :
                                <Alert onClose={handleClose} severity="info" sx={{ width: '100%' }}>
                                    This is an info message!
                                </Alert>
            }
        </Snackbar>
    )
}

export default SnackBar;
