import React, { FunctionComponent } from 'react';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';
import FormLabel from '@mui/material/FormLabel';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Divider from '@mui/material/Divider';
import CheckBoxWrapper from '../CheckBox';

const CheckBoxGroup: FunctionComponent<any> = (props: any) => {
    return (
        <Box component="form" onSubmit={props.handlePermissions} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'start', justifyContent: 'start', width: '100%' }}>
            {
                props.data.map((module: any) => (
                    <FormControl key={module.module} sx={{ m: 3, width: '100%' }} component="fieldset" variant="standard">
                        <FormLabel sx={{ m: 2 }} component="legend">{module.module.toUpperCase()}</FormLabel>
                        <FormGroup sx={{ flexDirection: 'row', width: '100%' }}>
                            {
                                module.actions.map((actn: any) => {
                                    return (
                                        // <FormControlLabel sx={{ alignSelf: 'start' }} key={Object.keys(actn)[0]}
                                        //     control={
                                        //         <Checkbox key={Object.keys(actn)[0]} onChange={props.handleChange} checked={Boolean(Object.values(actn)[0])} name={Object.keys(actn)[0]} />
                                        //     }
                                        //     label={Object.keys(actn)[0]}
                                        // />
                                        <CheckBoxWrapper
                                            key={Object.keys(actn)[0]}
                                            name={Object.keys(actn)[0]}
                                            label={Object.keys(actn)[0]}
                                            checked={Boolean(Object.values(actn)[0])} />
                                    )
                                })
                            }
                        </FormGroup>
                    </FormControl>

                ))
            }
            <Divider />
        </Box>
    )
}

export default CheckBoxGroup;