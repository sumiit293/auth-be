import Close from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import React, { FunctionComponent } from 'react';

const ConfirmDialog: FunctionComponent<any> = (props: any) => {
    const { open, handleConfirm, handleClose } = props;
    return (
        <Dialog open={open} maxWidth="sm" fullWidth>
            <DialogTitle>Confirm</DialogTitle>
            <Box position="absolute" onClick={() => handleClose(false)} sx={{margin: '2px'}} top={0} right={0}>
                <IconButton>
                    <Close />
                </IconButton>
            </Box>
            <DialogContent>
                <Typography>Are you sure you want to delete this item?</Typography>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => handleClose(false)} color="primary" variant="contained">
                    Cancel
                </Button>
                <Button onClick={() => handleConfirm(true)} color="warning" variant="contained">
                    Confirm
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ConfirmDialog;