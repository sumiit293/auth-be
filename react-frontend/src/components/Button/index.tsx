import React, { FunctionComponent } from 'react';
import Button from '@mui/material/Button';
import { useField, useFormikContext } from 'formik';

const ButtonWrapper: FunctionComponent<any> = ({ children, ...otherprops }: any) => {

    const { submitForm } = useFormikContext();

    const handleSubmit = () => {
        submitForm();
    }

    const configButton = {
        ...otherprops,
        onClick: handleSubmit,
        color: 'primary',
        // variant: 'contained'
    }

    return (
        <Button
            {...configButton}
        >
            {children}
        </Button>
    )
}

export default ButtonWrapper;
