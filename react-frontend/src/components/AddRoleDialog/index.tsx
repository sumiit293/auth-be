import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useCallback, useEffect, useState } from 'react';
import { addRoleAsync } from '../../redux/services/roles-service';
import { getPermissionsByRoleAsync, getStaticPermissionsAsync } from '../../redux/services/permissions-service';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import Divider from '@mui/material/Divider';
import TextFieldWrapper from '../TextField';
import * as yup from 'yup';
import Grid from '@mui/material/Grid';
import { Typography } from '@mui/material';
import { Formik, Form, Field } from 'formik';
import CheckBoxWrapper from '../CheckBox';
import ButtonWrapper from '../Button';

export interface AddRoleDialogProps {
    open: boolean;
    onSubmit: (data: any) => void;
    onClose: () => void;
}

const INITIAL_FORM_STATE = {
    name: '',
    permissions: []
}

const FORM_VALIDATION = yup.object().shape({
    name: yup.string().required('Required'),
    permissions: yup.array().of(yup.object().shape({
        module: yup.string(),
        actions: yup.array(),
    })).required('Required'),
});

const AddRoleDialog = (props: any) => {
    const [role, setRole] = useState('')
    const { onClose, open, onSubmit, type, roleData } = props;
    const user = useSelector((state: RootState) => state.auth.user);
    const [permissions, setPermissions]: any = useState([]);
    const [formValues, setFormValues]: any = useState(INITIAL_FORM_STATE);

    const getStaticPermissions = useCallback(async () => {
        const { permissionActions } = await getStaticPermissionsAsync(user.token);
        const modifiedPermissions = permissionActions.map((module: any) => {
            const modifiedActions = module.actions.map((actn: any) => {
                if (module.module === 'public') {
                    return { [actn]: true }
                } else {
                    return { [actn]: false }
                }
            })
            return { ...module, actions: modifiedActions }
        })
        setPermissions(modifiedPermissions);
        setFormValues({
            name: '',
            permissions: modifiedPermissions
        })
    }, [setPermissions, user])

    const getStaticAndRolePermissions = useCallback(async () => {
        // const { permissionActions } = await getStaticPermissionsAsync(user.token);
        const permissionsData = await getPermissionsByRoleAsync(roleData.id, user.token);
        // const res = permissionActions.filter((item1: any) => 
        //     !permissionsData.permissions.some((item2: any) => (item2.module === item1.module || item2.actions.length === item1.actions.length)))
        // console.log(res)
        setPermissions(permissionsData.permissions);
        setRole(roleData.name)
        setFormValues({
            name: roleData.name,
            permissions: permissionsData.permissions
        })
    }, [setPermissions, user, roleData, setFormValues, setRole])

    useEffect(() => {
        if (type === 'add') {
            getStaticPermissions();
        } else {
            getStaticAndRolePermissions()
        }
    }, [getStaticPermissions, type, getStaticAndRolePermissions])

    const handleClose = () => {
        onClose();
    };

    const handleRoleFormSubmit = async (data: any) => {
        // try {
        //     if (role) {
        //         const data = { name: role }
        //         const addedRole = await addRoleAsync(user.token, data);
        //         onSubmit(addedRole)
        //     } else {
        //         onClose();
        //     }
        // } catch (err) {

        // }
        console.log(data)
    };

    return (
        <Dialog onClose={handleClose} open={open} PaperProps={{ sx: { minWidth: "30%" } }}>
            <DialogTitle>Add Role</DialogTitle>

            <Formik
                initialValues={formValues}
                validationSchema={FORM_VALIDATION}
                onSubmit={handleRoleFormSubmit}
                enableReinitialize
            >
                <Form>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextFieldWrapper
                                    name="name"
                                    label="Name"
                                    value={role}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Divider />
                            </Grid>
                            {
                                permissions.map((module: any, i: number) => {
                                    return (
                                        <Grid item xs={12} key={module.module}>
                                            <Typography>{module.module}</Typography>
                                            <Field
                                                name={`permissions[${i}].module`}
                                                value={module.module} hidden={true} />
                                            {
                                                module.actions.map((actn: any, ind: number) => {
                                                    return (
                                                        <CheckBoxWrapper
                                                            key={Object.keys(actn)[0]}
                                                            name={`permissions[${i}].actions[${ind}][${Object.keys(actn)[0]}]`}
                                                            label={Object.keys(actn)[0]}
                                                            value={Boolean(Object.values(actn)[0])}
                                                            defaultChecked={Boolean(Object.values(actn)[0])}
                                                        />
                                                    )
                                                })
                                            }
                                        </Grid>
                                    )
                                })
                            }
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <ButtonWrapper>Submit</ButtonWrapper>
                    </DialogActions>
                </Form>
            </Formik>
        </Dialog>
    );


}



export default AddRoleDialog;
