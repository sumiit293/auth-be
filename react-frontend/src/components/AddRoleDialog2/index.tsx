import Close from '@mui/icons-material/Close';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Divider from '@mui/material/Divider';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import React, { FunctionComponent } from 'react'

const AddUpdateRole: FunctionComponent<any> = (props: any) => {
    const { onClose, open, onSubmit, setRole, role, handleActionChange, permissions } = props;

    return (
        <Dialog onClose={onClose} open={open} PaperProps={{ sx: { minWidth: "30%" } }}>
            <DialogTitle>Add Role</DialogTitle>
            <Box position="absolute" onClick={onClose} sx={{ margin: '2px' }} top={0} right={0}>
                <IconButton>
                    <Close />
                </IconButton>
            </Box>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField label='Name'
                            required
                            value={role}
                            onChange={(e: any) => setRole(e.target.value)}
                            fullWidth={true} variant='filled' color='primary' />
                    </Grid>
                    <Grid item xs={12}>
                        <Divider />
                    </Grid>
                    <Grid item xs={12}>
                        {
                            permissions && permissions?.permissions.map((module: any, i: number) => {
                                return (
                                    <Grid item xs={12} key={module.module}>
                                        <Typography sx={{ fontWeight: 'bold' }}>{module.module.charAt(0).toUpperCase() + module.module.slice(1)}</Typography>
                                        {
                                            module.actions.map((actn: any, ind: number) => {
                                                return (
                                                    <FormControl key={Object.keys(actn)[0]}>
                                                        <FormGroup>
                                                            <FormControlLabel
                                                                control={
                                                                    <Checkbox
                                                                        value={permissions.permissions[i].actions[ind][Object.keys(actn)[0]]}
                                                                        checked={permissions.permissions[i].actions[ind][Object.keys(actn)[0]]}
                                                                        onChange={() => handleActionChange(module.module, Object.keys(actn)[0])} />
                                                                }
                                                                label={Object.keys(actn)[0]}
                                                            />
                                                        </FormGroup>
                                                    </FormControl>
                                                )
                                            })
                                        }
                                        <Divider sx={{ marginBottom: '10px' }} />
                                    </Grid>
                                )
                            })
                        }
                    </Grid>
                </Grid>
            </DialogContent >
            <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button disabled={!role} onClick={onSubmit}>Submit</Button>
            </DialogActions>
        </Dialog >
    )
}

export default AddUpdateRole;