import { Card, CardContent, CardHeader, TableBody, TableCell, TableRow } from '@mui/material';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { addRoleAsync, deleteRoleAsync, getRoles, updateRoleAsync } from '../../redux/services/roles-service';
import useTable from '../../components/Table';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import { addPermissionsAsync, getPermissionsByRoleAsync, getStaticPermissionsAsync, updatePermissionsAsync } from '../../redux/services/permissions-service';
import DeleteOutline from '@mui/icons-material/DeleteOutline';
import EditOutlined from '@mui/icons-material/EditOutlined';
import SnackBar from '../../components/SnackBar';
import ConfirmDialog from '../../components/ConfirmDialog';
const AddRoleDialog2 = React.lazy(() => import('../../components/AddRoleDialog2'))

const data = [
    { id: "123456", name: "Admin" }
];

const headCells = [
    { label: "ID", id: "id" },
    { label: "Role", id: "name" },
    { label: "Actions", id: "action" },
];

const RolesListPage: FunctionComponent = () => {
    const [roles, setRoles] = useState(data);
    const [open, setOpen] = useState<Boolean>(false);
    const [roleData, setRoleData] = useState<any>();
    const [type, setType] = useState('add');
    const user = useSelector((state: RootState) => state.auth.user);
    const [role, setRole] = useState('')
    const [permissions, setPermissions]: any = useState()
    const [openSnack, setOpenSnack] = useState<Boolean>(false);
    const [snackMsg, setSnackMsg] = useState<string>('');
    const [snackType, setSnackType] = useState<string>('success');
    const [openConfirm, setOpenConfirm] = useState<Boolean>(false);

    const { TableDiv, TableHeader, TablePaginate,
        recordsAfterPagingAndSorting } = useTable(roles, headCells);

    const onClickRow = async (rowInfo: any) => {
        setRoleData(rowInfo);
        const staticAndRolePermissions = await getStaticAndRolePermissions(rowInfo.id);
        setRole(rowInfo.name)
        setPermissions(staticAndRolePermissions);
        setOpen(true);
        setType('update');
    };

    const handleOpen = () => {
        getStaticPermissions();
    };

    const handleSnackClose = () => {
        setOpenSnack(false);
    };

    const handleClose = () => {
        setOpen(false);
        setRole('')
        if (!open) {
            setType('add');
            setRoleData(undefined);
            setPermissions(undefined);
        }
    };

    const handleSubmit = async () => {
        setOpen(false);
        if (type === 'add') {
            await addRoleAndPermissions();

        } else {
            await updateRoleAndPerm();
        }
        setType('add');
        setRoleData(undefined);
        setRole('')
        setPermissions(undefined)
    };

    const getroles = useCallback(async () => {
        const { roles } = await getRoles(user.token);
        const modifiedRoles = roles.map((role: any) => {
            return {
                name: role.name,
                id: role._id
            }
        })
        setRoles(modifiedRoles);
    }, [setRoles, user.token])

    useEffect(() => {
        getroles();
    }, [getroles])

    const getStaticPermissions = async () => {
        try {
            const permissionsData = await getStaticPermissionsAsync(user.token);
            setPermissions({ permissions: permissionsData.permissions });
            setOpen(true);
            setType('add');
        } catch (err: any) {
            setSnackType('error')
            setSnackMsg(err.message ? err.message : 'Some Error Occurred')
            setOpenSnack(true)
        }
    }

    const getStaticAndRolePermissions = async (id: string) => {
        try {
            const permissionsData = await getPermissionsByRoleAsync(id, user.token);
            return permissionsData;
        } catch (err: any) {
            setSnackType('error')
            setSnackMsg(err.message ? err.message : 'Some Error Occurred')
            setOpenSnack(true)
        }

    }

    const handleActionChange = (module: string, action: string) => {
        const upadatedPermissions = permissions.permissions.map((perm: any) => {
            if (perm.module === module) {
                const updatedActions = perm.actions.map((actn: any) => {
                    if (Object.keys(actn)[0] === action) {
                        return {
                            [action]: !Object.values(actn)[0]
                        }
                    } else {
                        return actn;
                    }
                })
                return { ...perm, actions: updatedActions };
            } else {
                return perm;
            }
        })
        setPermissions({ ...permissions, permissions: upadatedPermissions })
    };

    const addRoleAndPermissions = async () => {
        try {
            const roleData = await addRoleAsync(user.token, { name: role })
            const existingRoles = roles;
            const addedRoleData = {
                name: roleData.role.name,
                id: roleData.role._id
            }
            existingRoles.push(addedRoleData)
            setRoles(existingRoles)
            await addPermissions(roleData.role._id)
            setSnackType('success')
            setSnackMsg('Successfully added')
            setOpenSnack(true)
        } catch (err: any) {
            setSnackType('error')
            setSnackMsg(err.message ? err.message : 'Some Error Occurred')
            setOpenSnack(true)
        }
    };

    const addPermissions = async (roleId: string) => {
        const data = {
            role: roleId,
            permissions: permissions.permissions
        }
        const permissionsData = await addPermissionsAsync(user.token, data)
        return permissionsData;
    };

    const updateRoleAndPerm = async () => {
        try {
            if (roleData.name !== role) {
                const roleUpdatedData = await updateRoleAsync(user.token, roleData.id, { name: role })
                const index = roles.findIndex(role => role.id === roleUpdatedData.role._id)
                const updatedRoles = roles;
                updatedRoles[index] = roleUpdatedData.role;
                setRoles(updatedRoles);
            }
            await updatePermissions()
            setSnackType('success')
            setSnackMsg('Successfully updated')
            setOpenSnack(true)
        } catch (err: any) {
            setSnackType('error')
            setSnackMsg(err.message ? err.message : 'Some Error Occurred')
            setOpenSnack(true)
        }
    };

    const updatePermissions = async () => {
        const permissionsData = await updatePermissionsAsync(user.token, permissions._id, permissions)
        return permissionsData;
    };

    const deleteRole = async () => {
        try {
            const roledeletedData = await deleteRoleAsync(user.token, roleData.id)
            const index = roles.findIndex(role => role.id === roledeletedData.role._id)
            const updatedRoles = roles;
            updatedRoles.splice(index, 1);
            setRoles(updatedRoles);
            setRoleData(undefined);
            setSnackType('success')
            setSnackMsg('Successfully deleted')
            setOpenConfirm(false)
            setOpenSnack(true)
        } catch (err: any) {
            setSnackType('error')
            setSnackMsg(err.message ? err.message : 'Some Error Occurred')
            setOpenSnack(true)
        }

    };

    const handleDelete = async (rowInfo: any) => {
        setRoleData(rowInfo);
        setOpenConfirm(true);
    };

    const handleDeleteConfirm = async (confirm: boolean) => {
        if (confirm) {
            await deleteRole();
        } else {
            setOpenConfirm(false);
        }
    };

    return (
        <Card>
            <CardHeader
                title="Roles" action=
                {
                    <Button onClick={handleOpen} variant="contained"
                        sx={{ marginRight: '10px', borderRadius: "5em", }} aria-label="add">
                        <AddIcon />
                    </Button>
                } />
            <AddRoleDialog2
                open={open}
                onClose={handleClose}
                onSubmit={handleSubmit}
                handleActionChange={handleActionChange}
                setRole={setRole}
                permissions={permissions}
                role={role}
                roleData={roleData} />
            <CardContent sx={{ height: 700, width: '100%' }}>
                <TableDiv>
                    <TableHeader />
                    <TableBody>
                        {
                            recordsAfterPagingAndSorting().map((role: any, i: number) => (
                                <TableRow key={i}>
                                    <TableCell>{role.id}</TableCell>
                                    <TableCell>{role.name}</TableCell>
                                    <TableCell>
                                        <EditOutlined sx={{ marginRight: '2px' }} onClick={() => onClickRow(role)} />
                                        <DeleteOutline onClick={() => handleDelete(role)} />
                                    </TableCell>

                                </TableRow>
                            ))
                        }
                    </TableBody>
                </TableDiv>
                <TablePaginate />
                <SnackBar open={openSnack} type={snackType} message={snackMsg} handleClose={handleSnackClose} />
                <ConfirmDialog open={openConfirm} handleClose={setOpenConfirm} handleConfirm={handleDeleteConfirm} />
            </CardContent>
        </Card>
    )
}

export default RolesListPage;
