import { Card, CardContent, CardHeader } from '@mui/material';
import React, { FunctionComponent, useCallback, useEffect, useState } from 'react';
import { DataGrid, GridColDef, GridRowsProp } from '@mui/x-data-grid';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { getRoles } from '../../redux/services/roles-service';

const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 100 },
    {
        field: 'name',
        headerName: 'Role',
        width: 150,
        editable: true,
    },
];

const rows: GridRowsProp = [
    {
        name: 'Super Admin',
        id: 12345,
    },
];

const RolesListPage: FunctionComponent = () => {
    const [roles, setRoles] = useState(rows);
    const user = useSelector((state: RootState) => state.auth.user);

    const onClickRow = (rowInfo: any) => {
        console.log(rowInfo);
    };

    const getroles = useCallback(async () => {
        const { roles } = await getRoles(user.token);
        const modifiedRoles = roles.map((role: any) => {
            return {
                name: role.name,
                id: role._id
            }
        })
        setRoles(modifiedRoles);
    }, [setRoles, user.token])

    useEffect(() => {
        getroles();
    }, [getroles])

    return (
        <Card>
            <CardHeader
                title="Roles" />
            <CardContent sx={{ height: 700, width: '100%' }}>
                <DataGrid
                    rows={roles}
                    columns={columns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    onRowClick={onClickRow}
                />
            </CardContent>
        </Card>
    )
}

export default RolesListPage;