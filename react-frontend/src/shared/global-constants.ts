
export class Globals {
    public static readonly SERVER_URL = 'http://localhost:8000/api';
    public static readonly API = {
        superadminCheck: Globals.SERVER_URL + '/auth/superadmincheck',
        superadminlogin: Globals.SERVER_URL + '/auth/superadminlogin',
        register: Globals.SERVER_URL + '/auth/register',
        permissions: Globals.SERVER_URL + '/permission',
        role: Globals.SERVER_URL + '/role',
        getStaticPermissions: Globals.SERVER_URL + '/permission/static',
        addPermissions: Globals.SERVER_URL + '/permission/add',
        getRoles: Globals.SERVER_URL + '/role',
        addRole: Globals.SERVER_URL + '/role/add',
    };
    public static readonly ROLE_NAMES = [
        { authenticated: 'Authenticated' },
        { superAdmin: 'Super Admin' },
        { public: 'Public' },
    ];
}
