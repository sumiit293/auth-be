import ActionTypes from "./action-types";

export const startUserLogin = (userDetails: any) => {
    return {
        type: ActionTypes.USER_LOGIN_START,
        userDetails
    }
}

export const userLogin = (user: any) => {
    return {
        type: ActionTypes.USER_LOGIN,
        user
    }
}

export const startUserRegister = (userDetails: any) =>{
    return {
        type: ActionTypes.USER_REGISTER_START,
        userDetails
    }
}

export const userRegister = (user: any) =>{
    return {
        type: ActionTypes.USER_REGISTER,
        user
    }
}

export const asyncError = (err: any) =>{
    return {
        type: ActionTypes.ASYNC_ERROR,
        error: err
    }
}
