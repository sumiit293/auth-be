import { userLoginStart, userRegisterStart } from './auth-saga';
import { all } from 'redux-saga/effects'

export default function* rootSaga() {
    yield all([
        userLoginStart(),
        userRegisterStart(),
    ])
}