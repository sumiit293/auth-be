import { put, takeLatest } from 'redux-saga/effects'
import ActionTypes from '../actions/action-types';
import { asyncError, userLogin, userRegister } from '../actions/auth-actions';
import { userLoginReq, userRegisterReq } from '../services/auth-service';


function* userLoginAsync(action: any) {
    try {
        const user: Object = yield userLoginReq(action.userDetails);
        yield put(userLogin(user));
    } catch (e: any) {
        console.log('e', e);
        yield put(asyncError(e.message));
    }
}

export function* userLoginStart() {
    yield takeLatest(ActionTypes.USER_LOGIN_START, userLoginAsync);
}

function* userRegisterAsync(action: any) {
    try {
        const user: Object = yield userRegisterReq(action.userDetails);
        yield put(userRegister(user));
    } catch (e: any) {
        console.log('e', e);
        yield put(asyncError(e.message));
    }
}

export function* userRegisterStart() {
    yield takeLatest(ActionTypes.USER_REGISTER_START, userRegisterAsync);
}
