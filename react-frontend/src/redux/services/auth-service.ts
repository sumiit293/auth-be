import { Globals } from "../../shared/global-constants";

export const userLoginReq = async (userDetails: any) => {
    try {
        const loginUser = await fetch(Globals.API.superadminlogin, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ...userDetails, returnSecureToken: true })
        });
        if (!loginUser.ok) {
            throw new Error('Something went wrong')
        }
        const user = await loginUser.json();
        return user;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const userRegisterReq = async (userDetails: any) => {
    try {
        const registerUser = await fetch(Globals.API.register, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ...userDetails, returnSecureToken: true })
        });
        if (!registerUser.ok) {
            throw new Error('Something went wrong')
        }
        const user = await registerUser.json();
        return user;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const superAdminCheck = async () => {
    try {
        const data = await fetch(Globals.API.superadminCheck, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const check = await data.json();
        return check;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}
