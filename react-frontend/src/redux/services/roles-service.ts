import { Globals } from "../../shared/global-constants";

export const getRoles = async (token: string) => {
    try {
        const data = await fetch(Globals.API.getRoles, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token, 
                'Content-Type': 'application/json'
            }
        });
        if (!data.ok) {
            throw new Error('Something went wrong');
        }
        const roles = await data.json();
        return roles;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const addRoleAsync = async (token: string, roleData: any) => {
    try {
        const data = await fetch(`${Globals.API.role}/add`, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token, 
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(roleData)
        });
        if (!data.ok) {
            throw new Error('Something went wrong')
        }
        const role = await data.json();
        return role;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const updateRoleAsync = async (token: string, roleId: string, roleData: any) => {
    try {
        const data = await fetch(`${Globals.API.role}/${roleId}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token, 
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(roleData)
        });
        if (!data.ok) {
            throw new Error('Something went wrong')
        }
        const role = await data.json();
        return role;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const deleteRoleAsync = async (token: string, roleId: string) => {
    try {
        const data = await fetch(`${Globals.API.role}/${roleId}`, {
            method: 'DELETE',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token, 
                'Content-Type': 'application/json'
            }
        });
        if (!data.ok) {
            throw new Error('Something went wrong')
        }
        const role = await data.json();
        return role;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}
