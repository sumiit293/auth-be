import { Globals } from "../../shared/global-constants";

export const getStaticPermissionsAsync = async (token: string) => {
    try {
        const data = await fetch(Globals.API.getStaticPermissions, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        });
        if (!data.ok) {
            throw new Error('Something went wrong');
        }
        const permissions = await data.json();
        return permissions;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const getPermissionsByRoleAsync = async (roleId: string, token: string) => {
    try {
        const data = await fetch(`${Globals.API.permissions}/role/${roleId}`, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            }
        });
        if (!data.ok) {
            throw new Error('Something went wrong');
        }
        const permissions = await data.json();
        return permissions.permissions;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const addPermissionsAsync = async (token: string, permissionDetails: any) => {
    try {
        const permissionsRes = await fetch(Globals.API.addPermissions, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(permissionDetails)
        });
        if (!permissionsRes.ok) {
            throw new Error('Something went wrong')
        }
        const permissions = await permissionsRes.json();
        return permissions;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}

export const updatePermissionsAsync = async (token: string, permissionId: string, permissionDetails: any) => {
    try {
        const permissionsRes = await fetch(`${Globals.API.permissions}/${permissionId}`, {
            method: 'PUT',
            mode: 'cors',
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(permissionDetails)
        });
        if (!permissionsRes.ok) {
            throw new Error('Something went wrong')
        }
        const permissions = await permissionsRes.json();
        return permissions;
    } catch (err) {
        console.log('err', err);
        throw err;
    }
}
