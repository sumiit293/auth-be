import ActionTypes from "../actions/action-types"

const initialState = {
    user: '',
    error: false,
    isLoading: false
}

const authReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case ActionTypes.USER_LOGIN_START:
            return {
                ...state,
                isLoading: true
            }
        case ActionTypes.USER_REGISTER_START:
            return {
                ...state,
                isLoading: true
            }
        case ActionTypes.USER_REGISTER:
            return {
                ...state,
                user: action.user,
                isLoading: false,
                error: false,
            }
        case ActionTypes.USER_LOGIN:
            return {
                ...state,
                user: action.user,
                isLoading: false,
                error: false,
            }
        default:
            return state
    }

}

export default authReducer;
