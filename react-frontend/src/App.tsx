import React from 'react';
import './App.css';
import {
  useRoutes,
} from 'react-router-dom';
import { privateRoutes, publicRoutes } from './layout/Router';
import { useSelector } from 'react-redux';
import { RootState } from './redux/store';

function App() {
  const user = useSelector((state: RootState) => state.auth.user);
  const routes = user ? privateRoutes : publicRoutes;
  const content = useRoutes(routes);
  
  return (
    <div className="App">
        {content}
    </div>
  );
}

export default App;
