import * as React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import AssignmentIcon from '@mui/icons-material/Assignment';
import menuItems from './PageMenuItems';
import { Link } from 'react-router-dom';

export const mainListItems = (
  menuItems.map((item: any, i: number) => {
    return (
      <div key={i}>
        <ListSubheader inset>{item.heading}</ListSubheader>
        {
          item.items.map((itm: any) => {
            return (
              <Link to={itm.link} key={itm.name} style={{ textDecoration: 'none', color: 'black' }}>
                <ListItem button>
                  <ListItemIcon>
                    <AssignmentIcon />
                  </ListItemIcon>
                  <ListItemText primary={itm.name} />
                </ListItem>
              </Link>

            )
          })
        }
      </div>
    )
  })
);