import { ReactNode } from 'react';

import BrightnessLowTwoToneIcon from '@mui/icons-material/BrightnessLowTwoTone';

export interface MenuItem {
    link?: string;
    icon?: ReactNode;
    badge?: string;
    items?: MenuItem[];
    name: string;
}

export interface MenuItems {
    items: MenuItem[];
    heading: string;
}

const menuItems: MenuItems[] = [
    {
        heading: 'Dashboard',
        items: [
            {
                name: 'Dashboard',
                link: '/dashboard',
                icon: <BrightnessLowTwoToneIcon />
            },
            {
                name: 'Roles List',
                link: '/dashboard/roles',
                icon: <BrightnessLowTwoToneIcon />
            },
        ]
    },
    // {
    //     heading: 'Example',
    //     items: [
    //         {
    //             name: 'Example',
    //             icon: <BrightnessLowTwoToneIcon />,
    //             items: [
    //                 {
    //                     name: 'Example',
    //                     link: '/example',
    //                     icon: <BrightnessLowTwoToneIcon />
    //                 },
    //             ]
    //         }
    //     ]
    // },
];

export default menuItems;
