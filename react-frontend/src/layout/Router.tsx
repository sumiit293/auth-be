import { Suspense, lazy } from 'react';

import Dashboard from './Dashboard';
import SuspenseLoader from '../components/SuspenseLoader';
import { Navigate } from 'react-router';

const Loader = (Component: any) => (props: any) => (
  <Suspense fallback={<SuspenseLoader />}>
    <Component {...props} />
  </Suspense>
);


// Pages

const DashboardPage = Loader(lazy(() => import('../pages/Dashboard/index')));
const LoginPage = Loader(lazy(() => import('../pages/SignIn/index')));
const RolesListPage = Loader(lazy(() => import('../pages/Roles-List2/index')));


export const publicRoutes = [
  {
    path: '/login',
    element: <LoginPage />,
    exact: true,
  },
  {
    path: '*',
    element: (
      <Navigate
        to="/login"
        replace
      />
    ),
  }
]

export const privateRoutes = [
  {
    path: 'dashboard',
    element: (
      <Dashboard />
    ),
    exact: true,
    children: [
      {
        path: '',
        element: <DashboardPage />,
        exact: true,
      },
      {
        path: 'roles',
        element: <RolesListPage />,
        exact: true,
      },
      {
        path: '*',
        element: (
          <Navigate
            to="/dashboard"
            replace
          />
        ),
      }
    ]
  },
  {
    path: '*',
    element: (
      <Navigate
        to="/dashboard"
        replace
      />
    ),
  }
]