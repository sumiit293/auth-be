const mongoose = require('mongoose')

const connectionUrl = `${process.env.DEV_DB_STRING}`;

const mongooseConnection = mongoose.connect(connectionUrl, {
	useNewUrlParser: true, 
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: false
})

module.exports = mongooseConnection;
