const express = require("express");
require('dotenv').config()
require('./server.js');
const cors = require('cors');
const Logger = require('./shared/middlewares/logger');
const morganMiddleware = require('./shared/middlewares/http-logger');
const ejs = require('ejs');
var bodyParser = require('body-parser')

// routes
const userRoutes = require('./api/users/user-routes');
const roleRoutes = require('./api/roles/role-routes');
const permissionRoutes = require('./api/permissions/permission-routes');
const categoryRoutes = require('./api/category/category-routes');
const productRoutes = require("./api/product/product-routes");

const port = process.env.PORT
const app = express()

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use(morganMiddleware)

app.use('/api/auth', userRoutes);
app.use('/api/role', roleRoutes);
app.use('/api/permission', permissionRoutes);
app.use('/api/product',productRoutes);
app.use('/api/category',categoryRoutes);

app.listen(port, () => {
	Logger.debug(`app is running on port ${port}`)
})